""" 
This program adds up integers that have
been passed as arguments in the command 
"""
import sys
try:
    total = sum(int(arg) for arg in sys.argv[1:])
    print('sum=', total)
except Exception as e:
    print('Error =>', str(e))