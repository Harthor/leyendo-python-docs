import re

def checkValue(value):
    if re.match(r'^\d{3}-\d{4}$', value):
        print(value, 'is a valid number...')
    else:
        print(value, 'is NOT valid')

arrValues = ['555-1212', 'ILL-EGAL']
newValue = input('Add new value to array\n')
arrValues.append(newValue)

for value in arrValues:
    checkValue(value)


    

    